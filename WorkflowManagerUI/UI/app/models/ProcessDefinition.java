/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import models.SourceSystem.ACTIVE_INACTIVE;
import net.sf.oval.constraint.MatchPattern;
import play.db.jpa.GenericModel;

/**
 *
 * @author kingd
 */
@Entity
@Table(name = "POC_PROCESS_DEF", catalog = "", schema = "LDHE_BIDS")

public class ProcessDefinition extends GenericModel implements Serializable {




	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(generator="MetaDataIdSeq", strategy=GenerationType.SEQUENCE)
	@SequenceGenerator(name="MetaDataIdSeq", sequenceName="LDHE_BIDS.POC_PROCESS_DEF_SEQ", allocationSize=20)
	@Basic(optional = false)
	@Column(name = "PROCESS_ID")
	private BigDecimal processId;
	@Basic(optional = false)
	@Column(name = "PROCESS_CD")
	private String processCd;
	@Basic(optional = false)
	@Column(name = "PROCESS_DESC")
	private String processDesc;
	@Column(name = "active")
	@Enumerated(EnumType.STRING)
	private ACTIVE_INACTIVE active;
	@JoinColumn(name = "source_system", referencedColumnName = "system_id")
	@ManyToOne(optional = false)
	private SourceSystem sourceSystem;
	
	 @OneToMany(cascade = CascadeType.ALL, mappedBy = "processDefinition")
	    private Collection<ProcessBPWMLog> logsCollection;
	 
	 @OneToMany(cascade = CascadeType.ALL, mappedBy = "processDefinition")
	    private Collection<ProcessRunStat> statsCollection;

	public Collection<ProcessBPWMLog> getLogsCollection() {
		return logsCollection;
	}

	public void setLogsCollection(Collection<ProcessBPWMLog> logsCollection) {
		this.logsCollection = logsCollection;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Column(name = "modify_user")
	private String modifyUser;
	@Column(name = "modify_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifyDate;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "processDefinition")
	private Collection<ProcessAction> processActionCollection;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "processDef")
	private Collection<ProcessDependencie> processDefinitionCollection;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "parentProcessDefinition")
	private Collection<ProcessDependencie> parentProcessDefinitionCollection;

	@Basic(optional = true)
	@Column(name = "CUTOFF_HOURS")
	@MatchPattern(pattern="^([0-9]|0[0-9]|1[0-9]|2[0-3])$",message="Please enter valid hour 0 to 23")
	private int cutOffHours;

	@Basic(optional = true)
	@Column(name = "CUTOFF_MINS")
	@MatchPattern(pattern="^[0-5][0-9]$",message="Please enter valid minute 0 to 59")
	private int cutOffMins;

	public Collection<ProcessAction> getProcessActionCollection() {
		return processActionCollection;
	}

	public void setProcessActionCollection(Collection<ProcessAction> processActionCollection) {
		this.processActionCollection = processActionCollection;
	}



	@PrePersist
	public void setDate() {
		this.modifyDate = new Date();
	}



	public BigDecimal getProcessId() {
		return processId;
	}



	public void setProcessId(BigDecimal processId) {
		this.processId = processId;
	}



	public String getProcessCd() {
		return processCd;
	}



	public void setProcessCd(String processCd) {
		this.processCd = processCd;
	}



	public String getProcessDesc() {
		return processDesc;
	}



	public void setProcessDesc(String processDesc) {
		this.processDesc = processDesc;
	}



	public SourceSystem getSourceSystem() {
		return sourceSystem;
	}



	public void setSourceSystem(SourceSystem sourceSystem) {
		this.sourceSystem = sourceSystem;
	}



	public String getModifyUser() {
		return modifyUser;
	}



	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}



	public Date getModifyDate() {
		return modifyDate;
	}



	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public ACTIVE_INACTIVE getActive() {
		return active;
	}



	public void setActive(ACTIVE_INACTIVE active) {
		this.active = active;
	}



	@Override
	public String toString() {
		return processCd;
	}

	public Collection<ProcessDependencie> getProcessDefinitionCollection() {
		return processDefinitionCollection;
	}

	public void setProcessDefinitionCollection(Collection<ProcessDependencie> processDefinitionCollection) {
		this.processDefinitionCollection = processDefinitionCollection;
	}

	public Collection<ProcessDependencie> getParentProcessDefinitionCollection() {
		return parentProcessDefinitionCollection;
	}

	public void setParentProcessDefinitionCollection(Collection<ProcessDependencie> parentProcessDefinitionCollection) {
		this.parentProcessDefinitionCollection = parentProcessDefinitionCollection;
	}

	public int getCutOffHours() {
		return cutOffHours;
	}

	public void setCutOffHours(int cutOffHours) {
		this.cutOffHours = cutOffHours;
	}

	public int getCutOffMins() {
		return cutOffMins;
	}

	public void setCutOffMins(int cutOffMins) {
		this.cutOffMins = cutOffMins;
	}	
}
