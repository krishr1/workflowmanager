/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import models.SourceSystem.ACTIVE_INACTIVE;
import play.db.jpa.GenericModel;


@Entity
@Table(name = "POC_PROCESS_ACTIONS", catalog = "", schema = "LDHE_BIDS")

public class ProcessAction extends GenericModel implements Serializable {
 

	private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator="MetaDataIdSeq", strategy=GenerationType.SEQUENCE)
    @SequenceGenerator(name="MetaDataIdSeq", sequenceName="LDHE_BIDS.POC_PROCESS_ACTIONS_SEQ", allocationSize=20)
    @Basic(optional = false)
    @Column(name = "ID")
    private BigDecimal Id;
    
    @Basic(optional = false)
    @Column(name = "ACTION_ATTRIBUTE")
    private String actionAtt;
    
   
    
    @Basic(optional = false)
    @Column(name = "ACTION_ATTRIBUTE_VAL")
    private String actionAttVal;
    
    @Column(name = "active")
    @Enumerated(EnumType.STRING)
    private ACTIVE_INACTIVE active;
    
    @JoinColumn(name = "ACTION_ID", referencedColumnName = "action_id")
    @ManyToOne(optional = false)
    private ActionDefinition actonId;
 
    @Column(name = "modify_user")
    private String modifyUser;
  
    @Column(name = "modify_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifyDate;

    @JoinColumn(name = "process_id", referencedColumnName = "process_id")
    @ManyToOne(optional = false)
    private ProcessDefinition processDefinition;
    
    
    
    public BigDecimal getId() {
		return Id;
	}



	public void setId(BigDecimal id) {
		Id = id;
	}



	public String getActionAtt() {
		return actionAtt;
	}



	public void setActionAtt(String actionAtt) {
		this.actionAtt = actionAtt;
	}



	public String getActionAttVal() {
		return actionAttVal;
	}



	public void setActionAttVal(String actionAttVal) {
		this.actionAttVal = actionAttVal;
	}

	


	



	public ActionDefinition getActonId() {
		return actonId;
	}



	public void setActonId(ActionDefinition actonId) {
		this.actonId = actonId;
	}



	public ProcessDefinition getProcessDefinition() {
		return processDefinition;
	}



	public void setProcessDefinition(ProcessDefinition processDefinition) {
		this.processDefinition = processDefinition;
	}



	@PrePersist
    public void setDate() {
    	this.modifyDate = new Date();
    }



	


	public String getModifyUser() {
		return modifyUser;
	}



	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}



	public Date getModifyDate() {
		return modifyDate;
	}



	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
	
	   public ACTIVE_INACTIVE getActive() {
			return active;
		}



		public void setActive(ACTIVE_INACTIVE active) {
			this.active = active;
		}



		@Override
		public String toString() {
			return processDefinition+" : "+actionAtt;
		}

		
}
