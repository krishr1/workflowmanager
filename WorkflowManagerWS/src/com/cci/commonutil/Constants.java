package com.cci.commonutil;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Constants {

	public static final SimpleDateFormat sfdate = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
	
	public static final SimpleDateFormat sfDateOnly = new SimpleDateFormat("MM/dd/yyyy");
	
	public static void main(String arga[]) throws ParseException {
		String date = "01/01/2017 03:26:35"; 
		System.out.println(sfdate.parse(date));
	}
}
