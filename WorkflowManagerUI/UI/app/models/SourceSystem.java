/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import play.db.jpa.GenericModel;
import play.mvc.Scope.Session;

/**
 *
 * @author kingd
 */
@Entity
@Table(name = "poc_source_system", catalog = "", schema = "LDHE_BIDS")

public class SourceSystem extends GenericModel implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator="MetaDataIdSeq", strategy=GenerationType.SEQUENCE)
    @SequenceGenerator(name="MetaDataIdSeq", sequenceName="LDHE_BIDS.poc_source_system_seq", allocationSize=20)
    @Basic(optional = false)
    @Column(name = "system_id")
    private BigDecimal systemId;
    @Column(name = "system_name")
    private String systemName;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sourceSystem")
    private Collection<ProcessDefinition> processDefinitionCollection;
    @Column(name = "active")
    @Enumerated(EnumType.STRING)
    private ACTIVE_INACTIVE active;
    @Column(name = "modify_user")
    private String modifyUser;
    @Column(name = "modify_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifyDate;
	public BigDecimal getSystemId() {
		return systemId;
	}
	public void setSystemId(BigDecimal systemId) {
		this.systemId = systemId;
	}
	public String getSystemName() {
		return systemName;
	}
	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}
	public Collection<ProcessDefinition> getProcessDefinitionCollection() {
		return processDefinitionCollection;
	}
	public void setProcessDefinitionCollection(Collection<ProcessDefinition> processDefinitionCollection) {
		this.processDefinitionCollection = processDefinitionCollection;
	}
	public ACTIVE_INACTIVE getActive() {
		return active;
	}
	public void setActive(ACTIVE_INACTIVE active) {
		this.active = active;
	}
	public String getModifyUser() {
		return modifyUser;
	}
	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}
	public Date getModifyDate() {
		return modifyDate;
	}
	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
    
    
@PrePersist
public void setDate() {
	this.modifyDate = new Date();
	this.modifyUser = Session.current().get("username");
}


public enum ACTIVE_INACTIVE { Y, N }

public enum SUCCESS_FAILURE { SUCCESS, FAILURE }

@Override
public String toString() {
	return this.systemName;
}
}
