package com.cci.bean;

import java.util.Date;

public class ProcessRunStat {

	private int processId;

	private String processName;
	
	private String active;

	private String rerun;

	private String modifyUser;

	private Date asOfDate;

	private Date startTime;

	private Date endTime;

	private int rowCount;

	@Override
	public String toString() {
		return "ProcessRunStat [processId=" + processId + ", processName=" + processName + ", asOfDate=" + asOfDate
				+ ", startTime=" + startTime + ", endTime=" + endTime + "]";
	}

	private String status;
	
	private int cuttOffHrs;
	
	private int cuttOffMins;

	public int getProcessId() {
		return processId;
	}

	public void setProcessId(int processId) {
		this.processId = processId;
	}
	
	public String getProcessName() {
		return processName;
	}

	public void setProcessName(String processName) {
		this.processName = processName;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getRerun() {
		return rerun;
	}

	public void setRerun(String rerun) {
		this.rerun = rerun;
	}

	public String getModifyUser() {
		return modifyUser;
	}

	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}

	public Date getAsOfDate() {
		return asOfDate;
	}

	public void setAsOfDate(Date asOfDate) {
		this.asOfDate = asOfDate;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public int getRowCount() {
		return rowCount;
	}

	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getCuttOffHrs() {
		return cuttOffHrs;
	}

	public void setCuttOffHrs(int cuttOffHrs) {
		this.cuttOffHrs = cuttOffHrs;
	}

	public int getCuttOffMins() {
		return cuttOffMins;
	}

	public void setCuttOffMins(int cuttOffMins) {
		this.cuttOffMins = cuttOffMins;
	}

	
}
