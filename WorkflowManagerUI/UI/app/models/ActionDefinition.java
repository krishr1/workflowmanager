/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import models.SourceSystem.ACTIVE_INACTIVE;
import play.db.jpa.GenericModel;

/**
 *

 */
@Entity
@Table(name = "POC_ACTION_DEFINITIONS", catalog = "", schema = "LDHE_BIDS")

public class ActionDefinition extends GenericModel implements Serializable {


	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(generator="MetaDataIdSeq", strategy=GenerationType.SEQUENCE)
	@SequenceGenerator(name="MetaDataIdSeq", sequenceName="LDHE_BIDS.POC_ACTION_DEFINITIONS_SEQ", allocationSize=20)
	@Basic(optional = false)
	@Column(name = "ACTION_ID")
	private BigDecimal actionId;

	@Basic(optional = false)
	@Column(name = "ACTION_CD")
	private String actionCd;

	@Basic(optional = false)
	@Column(name = "ACTION_DESC")
	private String actionDesc;

	@Column(name = "active")
	@Enumerated(EnumType.STRING)
	private ACTIVE_INACTIVE active;


	@Column(name = "modify_user")
	private String modifyUser;

	@Column(name = "modify_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifyDate;

	
	 @OneToMany(cascade = CascadeType.ALL, mappedBy = "actonId")
	    private Collection<ProcessAction> processActionCollection;
	 
	 @OneToMany(cascade = CascadeType.ALL, mappedBy = "actionDefinition")
	    private Collection<ProcessBPWMLog> logsCollection;
	
	public Collection<ProcessBPWMLog> getLogsCollection() {
		return logsCollection;
	}

	public void setLogsCollection(Collection<ProcessBPWMLog> logsCollection) {
		this.logsCollection = logsCollection;
	}

	public Collection<ProcessAction> getProcessActionCollection() {
		return processActionCollection;
	}

	public void setProcessActionCollection(Collection<ProcessAction> processActionCollection) {
		this.processActionCollection = processActionCollection;
	}

	@PrePersist
	public void setDate() {
		this.modifyDate = new Date();
	}

	public String getModifyUser() {
		return modifyUser;
	}



	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}



	public Date getModifyDate() {
		return modifyDate;
	}



	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public ACTIVE_INACTIVE getActive() {
		return active;
	}



	public void setActive(ACTIVE_INACTIVE active) {
		this.active = active;
	}



	@Override
	public String toString() {
		return actionCd;
	}

	public BigDecimal getActionId() {
		return actionId;
	}

	public void setActionId(BigDecimal actionId) {
		this.actionId = actionId;
	}

	public String getActionCd() {
		return actionCd;
	}

	public void setActionCd(String actionCd) {
		this.actionCd = actionCd;
	}

	public String getActionDesc() {
		return actionDesc;
	}

	public void setActionDesc(String actionDesc) {
		this.actionDesc = actionDesc;
	}

	 
}
