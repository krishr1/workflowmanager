package com.cci.action;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;

import com.cci.bean.BpwmLogs;
import com.cci.commonutil.DBUtil;
import com.cci.commonutil.PropertiesReader;
import com.cci.jobs.ProcessDependenciesJob;

public class LogAction implements Runnable{

	public static LinkedBlockingQueue<BpwmLogs> LOG_QUEUE = new LinkedBlockingQueue<BpwmLogs>();
	private static final Logger logger = Logger.getLogger(LogAction.class);
	@Override
	public void run() {
		// TODO Auto-generated method stub
		while(true) {
			try {
				BpwmLogs log = LOG_QUEUE.take();
				String insertQuery = PropertiesReader.getPropValues("insertBpwmLogs");
				Connection conn = null;
				PreparedStatement pstmt = null;
				
				try {
					conn = DBUtil.getPooledDBConn();
					pstmt = conn.prepareStatement(insertQuery);
					pstmt.setInt(1,log.getProcessId());
					pstmt.setInt(2,log.getActionId());
					pstmt.setString(3,log.getMessage());
					pstmt.setString(4,log.getModifyUser());
					pstmt.setDate(5,new java.sql.Date(log.getAsOfDate().getTime()));
					pstmt.execute();
					conn.commit();
					
				}
				catch(Exception ex) {
					logger.error("Exception while inserting into BPWM LOGS",ex);
				}
				finally {
					DBUtil.close(pstmt,null,conn);
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				logger.error("Exception while inserting into BPWM LOGS",e);
			}
		}
	}
	
	
}
