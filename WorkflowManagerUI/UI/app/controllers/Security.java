package controllers;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

import org.eclipse.jdt.core.dom.SuperConstructorInvocation;

import play.Logger;
import play.Play;

public class Security extends Secure.Security {

	static boolean authenticate(String username, String password) {
		return checkLDAP(username, password);
	}

	static void onDisconnect() {

	}
	
	

	public static boolean checkLDAP(String username, String password) {
		
		/*
		boolean returnFlag = false;
		Hashtable env = new Hashtable();
		String adminName = "ldhenergy\\" + username;
		String adminPassword = password;
		String ldapURL = Play.configuration.getProperty("ldap.url");
		env.put(Context.INITIAL_CONTEXT_FACTORY,
				"com.sun.jndi.ldap.LdapCtxFactory");
		// set security credentials, note using simple cleartext authentication
		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		env.put(Context.SECURITY_PRINCIPAL, adminName);
		env.put(Context.SECURITY_CREDENTIALS, adminPassword);

		// connect to my domain controller
		env.put(Context.PROVIDER_URL, ldapURL);

		try {
			// Create the initial directory context
			LdapContext ctx = new InitialLdapContext(env, null);
			// Create the search controls
			SearchControls searchCtls = new SearchControls();

			// Specify the search scope
			searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

			// specify the LDAP search filter
			String searchFilter = "(&(objectClass=user)(sAMAccountName="
					+ username + "))";

			// Specify the Base for the search
			String searchBase = "DC="
					+ Play.configuration.getProperty("ldap.domain") + ",DC="
					+ Play.configuration.getProperty("ldap.domain.suffix") + "";

			// initialize counter to total the group members
			int totalResults = 0;

			// Specify the attributes to return
			String returnedAtts[] = { "memberOf" };
			searchCtls.setReturningAttributes(returnedAtts);

			// Search for objects using the filter
			NamingEnumeration answer = ctx.search(searchBase, searchFilter,
					searchCtls);

			// Loop through the search results
			while (answer.hasMoreElements()) {
				SearchResult sr = (SearchResult) answer.next();

				// System.out.println(">>>" + sr.getName());

				// Print out the groups

				Attributes attrs = sr.getAttributes();
				if (attrs != null) {

					try {
						for (NamingEnumeration ae = attrs.getAll(); ae
								.hasMore();) {
							Attribute attr = (Attribute) ae.next();
							for (NamingEnumeration e = attr.getAll(); e
									.hasMore(); totalResults++) {
								String nxt = e.next().toString();
								if (nxt.contains(Play.configuration
										.getProperty("ldap.group")))
									returnFlag = true;
							}

						}

					} catch (NamingException e) {
						System.err.println("Problem listing membership: " + e);
					}

				}
			}

			ctx.close();

		}

		catch (NamingException e) {
			System.err.println("Problem searching directory: " + e);
		}*/
		//return returnFlag;
		return true;
	}

}
