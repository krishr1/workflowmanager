package com.cci.pluginimpl;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import org.apache.log4j.Logger;

import com.cci.bean.ActionAttributeVal;
import com.cci.jobs.ProcessDependenciesJob;
import com.cci.plugin.Plugin;
import com.cci.util.EmailUtil;

public class FilePlugin implements Plugin{

	private static final Logger logger = Logger.getLogger(FilePlugin.class);
	@Override
	public boolean execute(List<ActionAttributeVal> valList) {
		// TODO Auto-generated method stub
		String fileDest = "";
		String fileContent = "";
		
		for(ActionAttributeVal val:valList) {
			if(val.getActionName().equalsIgnoreCase("FILE_CREATE"))
			{
				if(val.getActionAttribute().equalsIgnoreCase("DEST_FILE"))
					fileDest = val.getActionAttributeVal();
				if(val.getActionAttribute().equalsIgnoreCase("FILE_CONTENT"))
					fileContent = val.getActionAttributeVal();
				
				
			}
		}
		System.out.println("Sending email....");
		
		try{
		    PrintWriter writer = new PrintWriter(fileDest, "UTF-8");
		    writer.println(fileContent);
		    
		    writer.close();
		} catch (IOException e) {
		   // do something
			logger.error("Error occurred while writing to file ",e);
		}
		return true;
	}

}
