package com.cci.jobs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.cci.action.ProcessAction;
import com.cci.commonutil.DBUtil;
import com.cci.commonutil.PropertiesReader;

public class ProcessActionJob implements Runnable{
	
	private static final Logger logger = Logger.getLogger(ProcessActionJob.class);
	public void run() {
		while(true) {
			
			try {
				
			List<HashMap<String,Object>> processDetails = ProcessDependenciesJob.ACTION_QUEUE.take();
			for(HashMap<String,Object> row:processDetails)  {
			int parentProcessId = (int) row.get("PARENT_PROCESS_ID");
			Date asOfDate = (Date) row.get("ASOF_DATE");
			String modifyUser = (String) row.get("MODIFY_USER");
			if(parentProcessId !=0) {
				String getDependentProcessesQuery = PropertiesReader.getPropValues("getAllDependentProcesses");
				Connection conn = null;
				PreparedStatement pstmt = null;
				ResultSet rs = null;
				try {
					conn = DBUtil.getPooledDBConn();
					pstmt = conn.prepareStatement(getDependentProcessesQuery);
					pstmt.setInt(1, parentProcessId);
					rs = pstmt.executeQuery();
					int failCount = 0;
					while(rs.next())
					{
						int processId = rs.getInt(1);
						String getProcessStatusQuery = PropertiesReader.getPropValues("getProcessCompletionCount");
						pstmt = conn.prepareStatement(getProcessStatusQuery);
						pstmt.setInt(1, processId);
						pstmt.setDate(2, new java.sql.Date(asOfDate.getTime()));
						ResultSet rs1 = pstmt.executeQuery();
						
						while(rs1.next()) {
							int count = rs1.getInt(1);
							if(count == 0)
								failCount += 1;
						}
						DBUtil.close(null, rs1, null);
					}
					if(failCount > 0)
					{
						System.out.println("something did not complete");
					}
					else
					{
						//Perform actions defined for this job
						ProcessAction.performActionsForProcess(parentProcessId,asOfDate,modifyUser);
					}
				}
				catch(Exception ex) {
					logger.error("Error encountered while performing process action. ");
				}
				finally {
					DBUtil.close(pstmt, rs, conn);
				}
			}
			}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				logger.error("Error encountered while performing process action. ");
			}
			
		}
	}

}
