/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import models.SourceSystem.ACTIVE_INACTIVE;
import models.SourceSystem.SUCCESS_FAILURE;
import play.db.jpa.GenericModel;

/**
 *

 */
@Entity
@Table(name = "POC_PROCESS_RUN_STATS", catalog = "", schema = "LDHE_BIDS")

public class ProcessRunStat extends GenericModel implements Serializable {


	@Override
	public String toString() {
		return "process=" + processDefinition + ", rerun=" + rerun
				+ ", modifyUser=" + modifyUser + ", modifyDate=" + modifyDate
				+ ", asOfDate=" + asOfDate + ", startTime=" + startTime
				+ ", endTime=" + endTime + ", rowCount=" + rowCount
				+ ", status=" + status + "";
	}

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(generator="MetaDataIdSeq", strategy=GenerationType.SEQUENCE)
	@SequenceGenerator(name="MetaDataIdSeq", sequenceName="LDHE_BIDS.POC_PROCESS_RUN_STATS_SEQ", allocationSize=20)
	@Basic(optional = false)
	@Column(name = "ID")
	private BigDecimal Id;

	
	@Column(name = "active")
	@Enumerated(EnumType.STRING)
	private ACTIVE_INACTIVE active;

	@Column(name = "rerun")
	@Enumerated(EnumType.STRING)
	private ACTIVE_INACTIVE rerun;


	@Column(name = "modify_user")
	private String modifyUser;

	@Column(name = "modify_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifyDate;

	@Column(name = "asofdate")
	private Date asOfDate;

	@Column(name = "Start_Time")
	private Date startTime;

	@Column(name = "end_time")
	private Date endTime;

	@Basic(optional = false)
	@Column(name = "row_Count")
	private BigDecimal rowCount;

	@Column(name = "status")
	@Enumerated(EnumType.STRING)
	private SUCCESS_FAILURE status;
	
	@JoinColumn(name = "process_id", referencedColumnName = "process_id")
	@ManyToOne(optional = false)
	private ProcessDefinition processDefinition;
	
	@PrePersist
	public void setDate() {
		this.modifyDate = new Date();
	}

	public String getModifyUser() {
		return modifyUser;
	}



	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}



	public Date getModifyDate() {
		return modifyDate;
	}



	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public ACTIVE_INACTIVE getActive() {
		return active;
	}


	public void setActive(ACTIVE_INACTIVE active) {
		this.active = active;
	}

	public BigDecimal getId() {
		return Id;
	}

	public void setId(BigDecimal id) {
		Id = id;
	}

	

	public ProcessDefinition getProcessDefinition() {
		return processDefinition;
	}

	public void setProcessDefinition(ProcessDefinition processDefinition) {
		this.processDefinition = processDefinition;
	}

	public ACTIVE_INACTIVE getRerun() {
		return rerun;
	}

	public void setRerun(ACTIVE_INACTIVE rerun) {
		this.rerun = rerun;
	}

	public Date getAsOfDate() {
		return asOfDate;
	}

	public void setAsOfDate(Date asOfDate) {
		this.asOfDate = asOfDate;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public BigDecimal getRowCount() {
		return rowCount;
	}

	public void setRowCount(BigDecimal rowCount) {
		this.rowCount = rowCount;
	}

	public SUCCESS_FAILURE getStatus() {
		return status;
	}

	public void setStatus(SUCCESS_FAILURE status) {
		this.status = status;
	}

}
