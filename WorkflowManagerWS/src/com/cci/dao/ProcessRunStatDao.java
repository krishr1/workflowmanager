package com.cci.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.cci.bean.ProcessRunStat;
import com.cci.commonutil.DBUtil;
import com.cci.commonutil.PropertiesReader;

public class ProcessRunStatDao {
	private static final Logger log = Logger.getLogger(ProcessRunStat.class);
	
	public boolean insert(ProcessRunStat processRunStatObj){
		log.info("Inside ProcessRunStatDao.Insert for : " + processRunStatObj.getProcessName());
		Connection con = null;
		PreparedStatement preStmt = null;
		try {
			con = DBUtil.getPooledDBConn();
			preStmt = con.prepareStatement(PropertiesReader.getPropValues("runstatInsertQuery"));

			preStmt.setInt(1, processRunStatObj.getProcessId());
			preStmt.setDate(2, new java.sql.Date(processRunStatObj.getAsOfDate().getTime()));
			preStmt.setTimestamp(3, new java.sql.Timestamp(processRunStatObj.getStartTime().getTime()));
			preStmt.setTimestamp(4, new java.sql.Timestamp(processRunStatObj.getEndTime().getTime()));
			preStmt.setInt(5, processRunStatObj.getRowCount());
			preStmt.setString(6, processRunStatObj.getRerun());			
			preStmt.setString(7, processRunStatObj.getModifyUser());
			preStmt.setString(8, processRunStatObj.getActive());
			preStmt.setString(9, processRunStatObj.getStatus());

			preStmt.execute();
			con.commit();
			return true;
		} catch (Exception e) {
			log.info("Exception while ProcessRunStatDao.Insert : ", e);
			return false;

		}
		finally {
			DBUtil.close(preStmt, null, con);
		}



	}


	public int retrieveCount(ProcessRunStat processRunStatObj){
		int count = 0;

		try{
			try(	
					Connection con = DBUtil.getPooledDBConn();
					PreparedStatement preStmt = createPreparedStatement(con, processRunStatObj);
					ResultSet res = preStmt.executeQuery();
					){

				while(res.next()){
					count = res.getInt("cnt");
				}
				log.info("Count for Rerun : " + count);


			} catch (Exception e) {
				log.info("Exception while ProcessRunStatDao.retrieveCount : ",e);
				return count;
			}
		}catch(Exception e)
		{
			log.info("Exception while ProcessRunStatDao.retrieveCount 2 : ",e);

		}
		return count;

	}

	private PreparedStatement createPreparedStatement(Connection con, ProcessRunStat processRunStatObj) throws SQLException{
		String sql = PropertiesReader.getPropValues("runstatSelQuery");
		PreparedStatement preStmt = con.prepareStatement(sql);
		preStmt.setInt(1, processRunStatObj.getProcessId());
		preStmt.setDate(2, new java.sql.Date(processRunStatObj.getAsOfDate().getTime()));

		return preStmt;
	}

	public ProcessRunStat retrieveProcessId(ProcessRunStat processRunStatObj){
		int count = 0;

		try{
			try(	
					Connection con = DBUtil.getPooledDBConn();
					PreparedStatement preStmt = createPreparedStatementProcessId(con, processRunStatObj);
					ResultSet res = preStmt.executeQuery();
					){

				while(res.next()){
					processRunStatObj.setProcessId(res.getInt("PROCESS_ID"));
					processRunStatObj.setCuttOffHrs(res.getInt("CUTTOFF_HOURS"));
					processRunStatObj.setCuttOffMins(res.getInt("CUTTOFF_MINS"));

				}
				log.info("Count for Rerun : " + count);


			} catch (Exception e) {
				log.info("Exception while ProcessRunStatDao.retrieveProcessId : ",e);
				return processRunStatObj;
			}
		}catch(Exception e)
		{
			log.info("Exception while ProcessRunStatDao.retrieveProcessId 2 : ",e);
		}
		return processRunStatObj;

	}
	private PreparedStatement createPreparedStatementProcessId(Connection con, ProcessRunStat processRunStatObj) throws SQLException{
		String sql = PropertiesReader.getPropValues("getProcessId");
		PreparedStatement preStmt = con.prepareStatement(sql);
		preStmt.setString(1, processRunStatObj.getProcessName());
		return preStmt;
	}
}


