package com.cci.jobs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.TimerTask;

import com.cci.bean.ProcessDef;
import com.cci.commonutil.DBUtil;
import org.apache.log4j.Logger;

import com.cci.commonutil.PropertiesReader;
import com.cci.util.EmailUtil;

public class CheckCutOffJob extends TimerTask{

	private static final Logger logger = Logger.getLogger(ProcessActionJob.class);
	@Override
	public void run() {
		String jobs ="";
		try {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		PreparedStatement pstmt1 = null;
		ResultSet rs1 = null;
		String cutOffJobsQuery = PropertiesReader.getPropValues("getCutoffJobs");
		try{
			conn = DBUtil.getPooledDBConn();
			pstmt = conn.prepareStatement(cutOffJobsQuery);
			rs = pstmt.executeQuery();
			int hours = 0;
			int minutes= 0;
			
			while(rs.next()) {
				ProcessDef def = new ProcessDef();
				def.setHours(rs.getInt("CUTOFF_HOURS"));
				def.setMinutes(rs.getInt("CUTOFF_MINS"));
				def.setProcessCd(rs.getString("PROCESS_CD"));
				def.setProcessId(rs.getInt("PROCESS_ID"));
				
				String innerQuery = PropertiesReader.getPropValues("getProcessCompletionCount");
				 pstmt1 = conn.prepareStatement(innerQuery);
				pstmt1.setInt(1,def.getProcessId());
				pstmt1.setDate(2, new java.sql.Date(new Date().getTime()));
				rs1 = pstmt1.executeQuery();
				int count = 0;
				while(rs1.next())
				{
					count = rs1.getInt(1);
				}
				if(count <= 0) 
				{
					
					jobs += ", "+def.getProcessCd();
					//EmailUtil.sendApplicationEmail(from, to, subject, body);
				}
				
			}
		}catch(Exception ex) {
			logger.error("Error encountered while checking job cut-off(SQL). ");
		}
		finally {
			DBUtil.close(pstmt1, rs1, null);
			DBUtil.close(pstmt,rs,conn);
		}
		}
		catch(Exception ex) {
			logger.error("Error encountered while checking job cut-off. ");
		}
		if(!jobs.equalsIgnoreCase("")) {
		String from = PropertiesReader.getPropValues("email.from.address");
		String to = PropertiesReader.getPropValues("email.to.address");
		String subject = PropertiesReader.getPropValues("email.cutoff.subject");
		String body = PropertiesReader.getPropValues("email.cutoff.body");
		body = body.replaceAll("%1", jobs);
		EmailUtil.sendApplicationEmail(from, to, subject, body);
		}
	}
}
