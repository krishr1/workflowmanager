package com.cci.messagedriven;

import java.sql.SQLException;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.QueueReceiver;
import javax.jms.TextMessage;

import org.apache.log4j.Logger;

import oracle.jms.AQjmsSession;



import com.cci.commonutil.DBUtil;
import com.cci.commonutil.PropertiesReader;
import com.cci.util.EmailUtil;
import com.cci.webservice.ServiceImpl;

/**
 * This is a message receiver class using the JMS Interface to AQ
 * 
 * 
 */
public class WorkflowJmsSubscriber extends AQApplication implements Runnable {
	private static final Logger logger = Logger.getLogger(DBUtil.class);
	

	/**
	 * The subscription id is assigned according command line arguments, set to
	 * 0 if not specified
	 */

	

	public WorkflowJmsSubscriber() {
		
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {

			JMSQueue aq = createQueueClient();
			startMonitor(aq);
			
		} catch (Exception ex) {
			System.err.println("AQJmsSubscriber.AQJmsSubscriber(): "
					+ ex.getMessage());
			ex.printStackTrace();
			
		}
	}

	/**
	 * Test method
	 * 
	 * @param session
	 *            Used AQ connection and session
	 * @exception javax.jms.JMSException
	 *                Super exception for all JMS errors
	 * @exception java.sql.SQLException
	 *                JDBC SQL exceptions
	 */
	protected void startMonitor(JMSQueue aq) throws JMSException, SQLException {

		// Start the connection
		//aq.connection.start();
		Queue queue = ((AQjmsSession) aq.session).getQueue(DB_AQ_ADMIN_NAME,
				PropertiesReader.getPropValues("WORKFLOW_QUEUE_NAME"));

		QueueReceiver subscriber = ((AQjmsSession) aq.session)
				.createReceiver(queue);
		logger.info("Started for queue " + PropertiesReader.getPropValues("WORKFLOW_QUEUE_NAME")
				);
		try {
			while (true) {
				try {
					TextMessage textMessage = (TextMessage) subscriber
							.receive(1); // Wait
											// in
											// milliseconds
					aq.connection.start();
					
					if (textMessage != null) {
						String messageData = (String) textMessage.getText();
						new ServiceImpl().ProcessWS(messageData);
						//executorService.submit(new PositionJob(messageData));
						
					}
				} catch (JMSException ex) {
					// Probably queues have been stopped.
					// Do Nothing.
					ex.printStackTrace();
					System.out.println("caught exception");
					aq.session.rollback();
					//Do not log here since it will fill up the log file.
				} finally {
					aq.session.commit();

				}

			}

		} catch (Exception ex) {
			EmailUtil.sendMail("Exception Caught while creating queue listener ","");
		//	aq.session.commit();

			// Close session
			aq.session.close();

			// Close connection
			aq.connection.close();
		}
		// Commit activities

	}

}

