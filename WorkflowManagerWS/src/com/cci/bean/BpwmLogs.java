package com.cci.bean;

import java.util.Date;

public class BpwmLogs {

	private int processId;
	private int actionId;
	private String message;
	private Date asOfDate;
	private String modifyUser;
	
	public String getModifyUser() {
		return modifyUser;
	}
	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}
	public Date getAsOfDate() {
		return asOfDate;
	}
	public void setAsOfDate(Date asOfDate) {
		this.asOfDate = asOfDate;
	}
	public int getProcessId() {
		return processId;
	}
	public void setProcessId(int processId) {
		this.processId = processId;
	}
	public int getActionId() {
		return actionId;
	}
	public BpwmLogs(int processId, int actionId, String message, Date asOfDate,String modifyUser) {
		super();
		this.processId = processId;
		this.actionId = actionId;
		this.message = message;
		this.asOfDate = asOfDate;
		this.modifyUser = modifyUser;
	}
	public void setActionId(int actionId) {
		this.actionId = actionId;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + actionId;
		result = prime * result + processId;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BpwmLogs other = (BpwmLogs) obj;
		if (actionId != other.actionId)
			return false;
		if (processId != other.processId)
			return false;
		return true;
	}
	
	
}
