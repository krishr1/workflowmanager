package com.cci.jobs;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;

import com.cci.bean.ProcessRunStat;
import com.cci.dao.ProcessDependenciesDAO;

public class ProcessDependenciesJob implements Runnable{
	private static final Logger logger = Logger.getLogger(ProcessDependenciesJob.class);
	public static LinkedBlockingQueue<List<HashMap<String,Object>>> ACTION_QUEUE = new LinkedBlockingQueue<List<HashMap<String,Object>>>(); 
	public void run() {
		while(true) {
			try {
			final ProcessRunStat stat = ProcessRunStatsJob.CHECK_QUEUE.take();
			final List<Integer> parentProcessId = ProcessDependenciesDAO.getParentProcess(stat.getProcessId());
			List<HashMap<String,Object>> tempList = new ArrayList<HashMap<String,Object>>();
			for(int t:parentProcessId) {
			HashMap<String,Object> tempMap = new HashMap<String,Object>();
			tempMap.put("PARENT_PROCESS_ID", t);
			tempMap.put("ASOF_DATE", stat.getAsOfDate());
			tempMap.put("MODIFY_USER", stat.getModifyUser());
			tempList.add(tempMap);
			}
			ACTION_QUEUE.put(tempList);
			}catch(Exception ex) {
				logger.error("Error occurred while performing action.");
			}
		}
	}
}
