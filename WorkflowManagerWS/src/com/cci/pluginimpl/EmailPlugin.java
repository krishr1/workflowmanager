package com.cci.pluginimpl;

import java.util.List;

import com.cci.bean.ActionAttributeVal;
import com.cci.plugin.Plugin;
import com.cci.util.EmailUtil;

public class EmailPlugin implements Plugin{

	
	@Override
	public boolean execute(List<ActionAttributeVal> valList) {
		// TODO Auto-generated method stub
		String from = "";
		String to = "";
		String subject = "";
		String body = "";
		for(ActionAttributeVal val:valList) {
			if(val.getActionName().equalsIgnoreCase("EMAIL"))
			{
				if(val.getActionAttribute().equalsIgnoreCase("FROM"))
					from = val.getActionAttributeVal();
				if(val.getActionAttribute().equalsIgnoreCase("TO"))
					to = val.getActionAttributeVal();
				if(val.getActionAttribute().equalsIgnoreCase("BODY"))
					body = val.getActionAttributeVal();
				if(val.getActionAttribute().equalsIgnoreCase("SUBJECT"))
					subject = val.getActionAttributeVal();
				
			}
		}
		System.out.println("Sending email....");
		EmailUtil.sendApplicationEmail(from, to, subject, body);
		return true;
	}

	
	
}
