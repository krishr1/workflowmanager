package com.cci.webservice;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface Service {

	@WebMethod
	public String ProcessWS(String params);
}
