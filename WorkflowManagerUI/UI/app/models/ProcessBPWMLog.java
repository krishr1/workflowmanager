package models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import models.SourceSystem.ACTIVE_INACTIVE;
import play.db.jpa.GenericModel;


@Entity
@Table(name = "POC_BPWM_LOGS", catalog = "", schema = "LDHE_BIDS")

public class ProcessBPWMLog extends GenericModel implements Serializable {

	
	@Id
	@GeneratedValue(generator="MetaDataIdSeq", strategy=GenerationType.SEQUENCE)
	@SequenceGenerator(name="MetaDataIdSeq", sequenceName="LDHE_BIDS.POC_BPWM_LOGS_SEQ", allocationSize=20)
	@Basic(optional = false)
	@Column(name = "LOG_ID")
	private BigDecimal Id;

	@Column(name = "message")
	private String message;
	
	
	@Column(name = "modify_user")
	private String modifyUser;

	@Column(name = "modify_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifyDate;
	
	@JoinColumn(name = "process_id", referencedColumnName = "process_id")
	@ManyToOne(optional = false)
	private ProcessDefinition processDefinition;
	
	@JoinColumn(name = "action_id", referencedColumnName = "action_id")
	@ManyToOne(optional = false)
	private ActionDefinition actionDefinition;

	public ProcessDefinition getProcessDefinition() {
		return processDefinition;
	}

	public void setProcessDefinition(ProcessDefinition processDefinition) {
		this.processDefinition = processDefinition;
	}

	public ActionDefinition getActionDefinition() {
		return actionDefinition;
	}

	public void setActionDefinition(ActionDefinition actionDefinition) {
		this.actionDefinition = actionDefinition;
	}

	public BigDecimal getId() {
		return Id;
	}

	public void setId(BigDecimal id) {
		Id = id;
	}

	

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getModifyUser() {
		return modifyUser;
	}

	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
	
	@Override
	public String toString() {
		return "Process  "+processDefinition+" performed action "+actionDefinition+" at "+modifyDate+" for user "+modifyUser;
	}
	
	
}
