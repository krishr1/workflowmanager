/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import models.SourceSystem.ACTIVE_INACTIVE;
import play.db.jpa.GenericModel;

/**
 *

 */
@Entity
@Table(name = "POC_PROCESS_DEPENDENCIES", catalog = "", schema = "LDHE_BIDS")
public class ProcessDependencie extends GenericModel implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "MetaDataIdSeq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "MetaDataIdSeq", sequenceName = "LDHE_BIDS.POC_PROCESS_DEPENDENCIES_SEQ", allocationSize = 20)
	@Basic(optional = false)
	@Column(name = "ID")
	private BigDecimal Id;

	@JoinColumn(name = "process_id", referencedColumnName = "process_id")
	@ManyToOne(optional = false)
	private ProcessDefinition processDef;

	@JoinColumn(name = "parent_process_id", referencedColumnName = "process_id")
	@ManyToOne(optional = false)
	private ProcessDefinition parentProcessDefinition;

	@Column(name = "active")
	@Enumerated(EnumType.STRING)
	private ACTIVE_INACTIVE active;

	@Column(name = "modify_user")
	private String modifyUser;

	@Column(name = "modify_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifyDate;

	@PrePersist
	public void setDate() {
		this.modifyDate = new Date();
	}

	public String getModifyUser() {
		return modifyUser;
	}

	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public ACTIVE_INACTIVE getActive() {
		return active;
	}

	public void setActive(ACTIVE_INACTIVE active) {
		this.active = active;
	}

	public BigDecimal getId() {
		return Id;
	}

	public void setId(BigDecimal id) {
		Id = id;
	}

	public ProcessDefinition getProcessDef() {
		return processDef;
	}

	public void setProcessDef(ProcessDefinition processDef) {
		this.processDef = processDef;
	}

	public ProcessDefinition getParentProcessDefinition() {
		return parentProcessDefinition;
	}

	public void setParentProcessDefinition(
			ProcessDefinition parentProcessDefinition) {
		this.parentProcessDefinition = parentProcessDefinition;
	}

	@Override
	public String toString() {
		return processDef + " >>>>> " + parentProcessDefinition;
	}

}
