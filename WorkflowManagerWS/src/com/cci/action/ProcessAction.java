package com.cci.action;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.cci.bean.ActionAttributeVal;
import com.cci.bean.BpwmLogs;
import com.cci.commonutil.DBUtil;
import com.cci.commonutil.PropertiesReader;
import com.cci.jobs.ProcessDependenciesJob;
import com.cci.plugin.Plugin;
import com.cci.pluginimpl.EmailPlugin;
import com.cci.pluginimpl.FilePlugin;
import com.cci.pluginimpl.NetworkFilePlugin;

public class ProcessAction {

	private static final Logger logger = Logger.getLogger(ProcessDependenciesJob.class);
	
	public static void performActionsForProcess(int processId,Date asOfDate,String modifyUser) {
		String getActionQuery = PropertiesReader.getPropValues("getActionsForProcess");
		Set<String> plugins = new HashSet<String>();
		Set<BpwmLogs> logs = new HashSet<BpwmLogs>();
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<ActionAttributeVal> valList = new ArrayList<ActionAttributeVal>();
		try {
			conn = DBUtil.getPooledDBConn();
			stmt = conn.prepareStatement(getActionQuery);
			stmt.setInt(1, processId);
			rs = stmt.executeQuery();
			while(rs.next()) {
				ActionAttributeVal val = new ActionAttributeVal();
				val.setActionName(rs.getString("ACTION_NAME"));
				val.setActionAttributeVal(rs.getString("ACTION_ATTRIBUTE_VAL"));
				val.setActionAttribute(rs.getString("ACTION_ATTRIBUTE"));
				val.setActionId(rs.getInt("ACTION_ID"));
				val.setProcessId(rs.getInt("PROCESS_ID"));
				valList.add(val);
			}
			
			//Loop through and get the unique list of action names
			  for(ActionAttributeVal attr:valList) {
				  plugins.add(attr.getActionName());
				  logs.add(new BpwmLogs(attr.getProcessId(),attr.getActionId(),attr.getActionName(),asOfDate,modifyUser));
			  }
			  
			  
			  //Insert into BPWM_LOGS
			  for(BpwmLogs log:logs)
			  {
				  LogAction.LOG_QUEUE.add(log);
			  }
			
			//Call the action plugin
			  for(String plugin:plugins)
			  {
				  if(plugin.equalsIgnoreCase("EMAIL"))
				  {
					 Plugin pg = new EmailPlugin();
					 pg.execute(valList);
				  }
				  if(plugin.equalsIgnoreCase("FILE_CREATE"))
				  {
					Plugin pg = new FilePlugin();
						 pg.execute(valList);
				  }
				  if(plugin.equalsIgnoreCase("NETWORK_FILE_CREATE"))
				  {
					Plugin pg = new NetworkFilePlugin();
						 pg.execute(valList);
				  }
			  }
			
		}catch(Exception ex) {
			logger.error("Error occurred while executing action.",ex);
		}
	}
}
