package com.cci.main;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.LinkedBlockingQueue;

import javax.xml.ws.Endpoint;

import org.apache.log4j.Logger;

import com.cci.action.LogAction;
import com.cci.bean.ProcessRunStat;
import com.cci.commonutil.PropertiesReader;
import com.cci.jobs.CheckCutOffJob;
import com.cci.jobs.ProcessActionJob;
import com.cci.jobs.ProcessDependenciesJob;
import com.cci.jobs.ProcessRunStatsJob;
import com.cci.messagedriven.WorkflowJmsSubscriber;
import com.cci.webservice.ServiceImpl;



public class Start {
	private static final Logger log = Logger.getLogger(Start.class);
	public static LinkedBlockingQueue<ProcessRunStat> STATS_QUEUE  = new LinkedBlockingQueue<ProcessRunStat>();
	public static void main(String args[]) throws IOException {
	
		log.info("Inside Start.main ");
		Thread jmsSubscriberThread =new Thread(new WorkflowJmsSubscriber());
		jmsSubscriberThread.start();
		Thread runStatsThread  = new Thread(new ProcessRunStatsJob());
		runStatsThread.start();
		Thread runDependenciesThread = new Thread(new ProcessDependenciesJob());
		runDependenciesThread.start();
		Thread runActionThread = new Thread(new ProcessActionJob());
		runActionThread.start();
		Thread logActionThread = new Thread(new LogAction());
		logActionThread.start();

		
		
		 TimerTask timerTask = new CheckCutOffJob();
	        //running timer task as daemon thread
	        Timer timer = new Timer(true);
	        timer.scheduleAtFixedRate(timerTask, 0, 60*1000);

		
		String portNumber = PropertiesReader.getPropValues("portNumber");
		
		Endpoint e = Endpoint.create(new ServiceImpl());
		String computername = InetAddress.getLocalHost().getHostName();
		String url = "http://" + computername + ":" + portNumber + "/" + "Service";
		e.publish(url);
		log.info("End point published");

		
	}
}
