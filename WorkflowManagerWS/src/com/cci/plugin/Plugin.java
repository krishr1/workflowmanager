package com.cci.plugin;

import java.util.List;

import com.cci.bean.ActionAttributeVal;

public interface Plugin {

	public boolean execute(List<ActionAttributeVal> valList);
}
