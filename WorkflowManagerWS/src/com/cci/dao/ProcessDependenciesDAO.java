package com.cci.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.cci.commonutil.DBUtil;
import com.cci.commonutil.PropertiesReader;

public class ProcessDependenciesDAO {

	private static final Logger logger = Logger.getLogger(ProcessDependenciesDAO.class);
	public static List<Integer> getParentProcess(int processID) {
		List<Integer> parentProcessId = new ArrayList<Integer>();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DBUtil.getPooledDBConn();
			pstmt = conn.prepareStatement(PropertiesReader.getPropValues("getParentProcessID"));
			pstmt.setInt(1, processID);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				parentProcessId.add(rs.getInt(1));
			}
		}catch(Exception ex) {
			logger.error("Exception occurred while getting parent process id",ex);
		}
		finally {
			DBUtil.close(pstmt, rs, conn);
		}
		return parentProcessId;
	}
	
	
		
}
