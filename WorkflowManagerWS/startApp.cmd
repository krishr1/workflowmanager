@ECHO OFF

set JAVA_HOME=C:\Java\jdk1.7.0_21
IF  "%JAVA_HOME%" == "" GOTO NO_JAVA 

:START_JAVA
@ECHO USING JAVA_HOME  %JAVA_HOME%
"%JAVA_HOME%/bin/java" -Xms128m -Xmx512m  -classpath "RiskWorkflow.jar;conf/riskpoc.properties;lib/*" com.cci.main.Start
ENDLOCAL
GOTO END

:NO_JAVA
@ECHO JAVA_HOME  NOT SET
GOTO END

:END
@PAUSE