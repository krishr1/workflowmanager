package com.cci.bean;

public class Email
{
    /** The attachment. */
    private String              attachment         = null;

    /** The attachment filename. */
    private String              attachmentFilename = null;

    /** The body. */
    private String              body               = null;

    /** The subject. */
    private String              subject            = null;
    
    private String				recipient		   = null;

    public String getAttachment()
    {
        return attachment;
    }

    public void setAttachment(String attachment)
    {
        this.attachment = attachment;
    }

    public String getAttachmentFilename()
    {
        return attachmentFilename;
    }

    public void setAttachmentFilename(String attachmentFilename)
    {
        this.attachmentFilename = attachmentFilename;
    }

    public String getBody()
    {
        return body;
    }

    public void setBody(String body)
    {
        this.body = body;
    }

    public void appendBody(String txt)
    {
        final StringBuilder report = new StringBuilder();
        report.append(this.body);
        report.append("\n");
        report.append(txt);
        report.append("\n");

        this.body = report.toString();
    }

    public String getSubject()
    {
        return subject;
    }

    public void setSubject(String subject)
    {
        this.subject = subject;
    }
    
    public String getRecipient()
    {
        return recipient;
    }

    public void setRecipient(String recipient)
    {
        this.recipient = recipient;
    }
}
