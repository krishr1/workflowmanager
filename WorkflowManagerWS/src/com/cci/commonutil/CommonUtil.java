package com.cci.commonutil;

import java.util.Date;

import org.apache.log4j.Logger;

public class CommonUtil {

	private static final Logger log = Logger.getLogger(CommonUtil.class);
	public static java.sql.Date convertDate(Date date){
		
		try {
			java.sql.Date sqlDate = new java.sql.Date(date.getTime());
			
			
			return sqlDate;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	
	public static void main(String agr[]) {
		System.out.println("Date : " + CommonUtil.convertDate(new Date()).getClass()); 	
	}
	
}
