package com.cci.jobs;

import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;

import com.cci.bean.ProcessRunStat;
import com.cci.dao.ProcessRunStatDao;
import com.cci.main.Start;

public class ProcessRunStatsJob implements Runnable{
	private static final Logger log = Logger.getLogger(ProcessRunStatsJob.class);
	public static LinkedBlockingQueue<ProcessRunStat> CHECK_QUEUE = new LinkedBlockingQueue<ProcessRunStat>();
	@Override
	public void run() {
		// TODO Auto-generated method stub
		while(true){
			ProcessRunStat stat = null;
			try {
				stat = Start.STATS_QUEUE.take();
				if(new ProcessRunStatDao().insert(stat))
				{
				CHECK_QUEUE.put(stat);
				}
				System.out.println("elemet  "+stat);
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
			
		}
			
	}
	

}
