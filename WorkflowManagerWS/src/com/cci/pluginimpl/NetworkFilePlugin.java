package com.cci.pluginimpl;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.util.List;

import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileOutputStream;

import com.cci.bean.ActionAttributeVal;
import com.cci.plugin.Plugin;

public class NetworkFilePlugin implements Plugin{

	@Override
	public boolean execute(List<ActionAttributeVal> valList) {
		// TODO Auto-generated method stub
		boolean successful = false;
		 try {
			 
			 String username = "";
				String password = "";
				String filepath = "";
				String fileContent = "";
				for(ActionAttributeVal val:valList) {
					if(val.getActionName().equalsIgnoreCase("NETWORK_FILE_CREATE"))
					{
						if(val.getActionAttribute().equalsIgnoreCase("USERNAME"))
							username = val.getActionAttributeVal();
						if(val.getActionAttribute().equalsIgnoreCase("PASSWORD"))
							password = val.getActionAttributeVal();
						if(val.getActionAttribute().equalsIgnoreCase("FILE_PATH"))
							filepath = val.getActionAttributeVal();
						if(val.getActionAttribute().equalsIgnoreCase("FILE_CONTENT"))
							fileContent = val.getActionAttributeVal();
						
					}
				}
		       String userPassword = username + ":" + password;
		     NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(
		               userPassword);
		     filepath = filepath.replace("\\\\", "");
		     filepath = filepath.replace("\\","/");
		     filepath = "smb://"+filepath;
		     
		        String path = filepath;
		        SmbFile sFile = new SmbFile(path, auth);
		        SmbFileOutputStream sfos = new SmbFileOutputStream(sFile);
		      sfos.write(fileContent.getBytes());
		     successful = true;
		      sfos.close();
		   } catch (Exception e) {
		     successful = false;
		     System.out.println("Problem in Creating New File :" + e);
		   }
		   return successful;
	}
	
	public static void main(String args[]) throws IOException {
		String filepath = "\\\\stmdvlp01\\ldhe\\cxl\\test1.txt";
		filepath = filepath.replace("\\\\", "");
	     filepath = filepath.replace("\\","/");
	     filepath = "smb://"+filepath;
	     System.out.println(filepath);
	     String fileContent = "";
	     String userPassword = "krishr" + ":" + "Mar$2017";
	     NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(
	               userPassword);
	     filepath = filepath.replace("\\\\", "");
	     filepath = filepath.replace("\\","/");
	     filepath = filepath;
	     
	        String path = filepath;
	        SmbFile sFile = new SmbFile(path, auth);
	        SmbFileOutputStream sfos = new SmbFileOutputStream(sFile);
	      sfos.write(fileContent.getBytes());
	    boolean successful = true;
		
	}
	

}
