package com.cci.commonutil;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class PropertiesReader {
	private static final Logger log = Logger.getLogger(PropertiesReader.class);
	private static Properties properties ;
	private static InputStream inputStream;

	static {
		try {
			properties = new Properties();
			inputStream = new FileInputStream("./conf/riskpoc.properties");

			if (inputStream != null) {
				properties.load(inputStream);
			} else {
				throw new FileNotFoundException("property file ./conf/riskpoc.properties not found in the classpath");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/*public static void loadProperties(String fileName) throws IOException{
		
	}*/

	public static String getPropValues(String propName) {
		
		// get the property value and print it out
		String value = properties.getProperty(propName);

		return value;
	}
}
