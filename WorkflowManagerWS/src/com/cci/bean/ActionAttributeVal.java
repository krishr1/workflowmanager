package com.cci.bean;

public class ActionAttributeVal {

	private String actionName;
	private String actionAttribute;
	private String actionAttributeVal;
	private int processId;
	private int actionId;
	
	public int getProcessId() {
		return processId;
	}
	public void setProcessId(int processId) {
		this.processId = processId;
	}
	public int getActionId() {
		return actionId;
	}
	public void setActionId(int actionId) {
		this.actionId = actionId;
	}
	public String getActionName() {
		return actionName;
	}
	public void setActionName(String actionName) {
		this.actionName = actionName;
	}
	public String getActionAttribute() {
		return actionAttribute;
	}
	public void setActionAttribute(String actionAttribute) {
		this.actionAttribute = actionAttribute;
	}
	public String getActionAttributeVal() {
		return actionAttributeVal;
	}
	public void setActionAttributeVal(String actionAttributeVal) {
		this.actionAttributeVal = actionAttributeVal;
	}
	
}
