package com.cci.webservice;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.jws.WebMethod;
import javax.jws.WebService;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import com.cci.bean.ProcessRunStat;
import com.cci.commonutil.Constants;
import com.cci.dao.ProcessRunStatDao;
import com.cci.main.Start;

@WebService(endpointInterface = "com.cci.webservice.Service")
public class ServiceImpl implements Service{
	private static final Logger log = Logger.getLogger(ServiceImpl.class);
	@Override
	@WebMethod
	public String ProcessWS(String payLoad) {

		log.info("Values Received : " + payLoad);
		ProcessRunStat runStatObj = new ProcessRunStat();
		ProcessRunStatDao runStatDaoObj = new ProcessRunStatDao();
		
		Map receivedData = new HashMap();
		try {
			JSONObject obj = new JSONObject(payLoad);
			
			receivedData.put("PROCESS_NAME", obj.getString("PROCESS_NAME"));
			receivedData.put("STATUS", obj.getString("STATUS"));
			receivedData.put("PROCESS_NAME", obj.getInt("ROW_COUNT"));
			receivedData.put("START_TIME", obj.getString("START_TIME"));
			receivedData.put("END_TIME", obj.getString("END_TIME"));
			receivedData.put("ASOFDATE", obj.getString("ASOFDATE"));
			
			runStatObj.setProcessName(obj.getString("PROCESS_NAME"));
			runStatObj.setStatus(obj.getString("STATUS"));
			runStatObj.setRowCount(obj.getInt("ROW_COUNT"));
			runStatObj.setStartTime(Constants.sfdate.parse(obj.getString("START_TIME")));
			runStatObj.setEndTime(Constants.sfdate.parse(obj.getString("END_TIME")));
			runStatObj.setAsOfDate(Constants.sfDateOnly.parse(obj.getString("ASOFDATE")));
			runStatObj.setModifyUser(obj.getString("USER"));
			
			runStatObj = runStatDaoObj.retrieveProcessId(runStatObj);
			if(runStatDaoObj.retrieveCount(runStatObj) > 0){
				runStatObj.setRerun("Y");
			}else{
				runStatObj.setRerun("N");
			}
			
			Start.STATS_QUEUE.put(runStatObj);
		} catch (Exception e) {
			log.info("Exception while ServiceImpl.ProcessWS ", e);
		}
		
		
		
		return "Success";
	}


}
