package com.cci.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import org.apache.log4j.Logger;

import com.cci.bean.Email;
import com.cci.commonutil.PropertiesReader;


/**
 * A Data Access Object (DAO) Implementation for Email.
 * 
 */
public class EmailUtil {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = Logger
			.getLogger(EmailUtil.class);

	/**
	 * Send mail.
	 * 
	 * @param email
	 *            the email
	 */
	public static void sendApplicationEmail(String from, String to, String subject, String body)
	{
		LOGGER.debug("EmailDao : sendMail() : Start sendMail()");

		try {
			

				
				// final String to =
				// ReportGenerator.properties.getProperty("email_to_address");

				// create some properties and get the default Session
				final Properties props = new Properties();

				LOGGER.debug(" : sendMail() : Host : {}"+ PropertiesReader.getPropValues("email.host"));

				props.put("mail.smtp.host", PropertiesReader.getPropValues("email.host"));
				props.put("mail.debug",PropertiesReader.getPropValues("email.debug"));

				final Session session = Session.getInstance(props, null);

				// create a message
				final MimeMessage msg = new MimeMessage(session);
				msg.setFrom(new InternetAddress(from));
				final InternetAddress[] address = InternetAddress.parse(to);
				msg.setRecipients(Message.RecipientType.TO, address);
				msg.setSubject(subject);

				// create and fill the first message part
				final MimeBodyPart mbp1 = new MimeBodyPart();
				System.out.println("email "+body);
				mbp1.setText(body);

				// create the second message part
				final MimeBodyPart mbp2 = new MimeBodyPart();

				final Multipart mp = new MimeMultipart();
				mp.addBodyPart(mbp1);

			/*	if (email.getAttachment() != null) {
					// attach the file to the message
					final DataSource ds = new ByteArrayDataSource(
							email.getAttachment(), "text/plain");
					mbp2.setDataHandler(new DataHandler(ds));
					mbp2.setFileName(email.getAttachmentFilename());

					// create the Multipart and add its parts to it
					mp.addBodyPart(mbp2);
				}*/
				// add the Multipart to the message
				msg.setContent(mp);

				// send the message
				LOGGER.info(" : sendMail() : Sending Email...");
				Transport.send(msg);
			
		} catch (final Exception e) {
			LOGGER.error(" : Exception caught invoking EmailDao()", e);
		}

		LOGGER.debug(" : sendMail() : End sendMail()");
	}
	

	public static void sendMail(String subject, String body) {
		try {
			final Email email = new Email();
			final SimpleDateFormat sdf = new SimpleDateFormat(
					"dd-MMM-yyyy kk:mm:ss");
			email.setRecipient(PropertiesReader.getPropValues("email.to.address"));
			email.setSubject(subject + " : " + sdf.format(new Date()));
			if(body == null)
			email.setBody("");
			else
			email.setBody(body);
			sendMail(email);
		} catch (Exception ex) {
			LOGGER.error("Error sending email");
		}
	}
	public static void sendMail(final Email email) {
		LOGGER.debug("EmailDao : sendMail() : Start sendMail()");

		try {
			

				final String from = PropertiesReader.getPropValues("email.from.address");
				// final String to =
				// ReportGenerator.properties.getProperty("email_to_address");

				// create some properties and get the default Session
				final Properties props = new Properties();

				LOGGER.debug(" : sendMail() : Host : {}"+ PropertiesReader.getPropValues("email.host"));

				props.put("mail.smtp.host", PropertiesReader.getPropValues("email.host"));
				props.put("mail.debug", PropertiesReader.getPropValues("email.debug"));

				final Session session = Session.getInstance(props, null);

				// create a message
				final MimeMessage msg = new MimeMessage(session);
				msg.setFrom(new InternetAddress(from));
				final InternetAddress[] address = InternetAddress.parse(email
						.getRecipient());
				msg.setRecipients(Message.RecipientType.TO, address);
				msg.setSubject(email.getSubject());

				// create and fill the first message part
				final MimeBodyPart mbp1 = new MimeBodyPart();
				System.out.println("email "+email.getBody());
				mbp1.setText(email.getBody());

				// create the second message part
				final MimeBodyPart mbp2 = new MimeBodyPart();

				final Multipart mp = new MimeMultipart();
				mp.addBodyPart(mbp1);

				if (email.getAttachment() != null) {
					// attach the file to the message
					final DataSource ds = new ByteArrayDataSource(
							email.getAttachment(), "text/plain");
					mbp2.setDataHandler(new DataHandler(ds));
					mbp2.setFileName(email.getAttachmentFilename());

					// create the Multipart and add its parts to it
					mp.addBodyPart(mbp2);
				}
				// add the Multipart to the message
				msg.setContent(mp);

				// send the message
				LOGGER.info(" : sendMail() : Sending Email...");
				Transport.send(msg);
			
		} catch (final Exception e) {
			LOGGER.error(" : Exception caught invoking EmailDao()", e);
		}

		LOGGER.debug(" : sendMail() : End sendMail()");
	}
}
