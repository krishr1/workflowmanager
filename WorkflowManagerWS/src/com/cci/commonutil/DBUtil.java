package com.cci.commonutil;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import oracle.jdbc.driver.OracleConnection;

public class DBUtil {
	private static final Logger logger = Logger.getLogger(DBUtil.class);


	private static OracleConnection CXLDBConn = null;
	private static Connection pooledCXLDBConn = null;
	private static ComboPooledDataSource cpds = null;

	static {
		try {
			cpds = new ComboPooledDataSource();
			cpds.setDriverClass("oracle.jdbc.driver.OracleDriver");
			cpds.setJdbcUrl(PropertiesReader.getPropValues("db_url"));
			cpds.setUser(PropertiesReader.getPropValues("DB_USER_NAME"));
			cpds.setPassword(PropertiesReader.getPropValues("DB_PASSWORD"));
			cpds.setInitialPoolSize(Integer.parseInt(PropertiesReader.getPropValues("INITIAL_POOL_SIZE")));
			cpds.setMinPoolSize(Integer.parseInt(PropertiesReader.getPropValues("MIN_POOL_SIZE")));
			cpds.setMaxPoolSize(Integer.parseInt(PropertiesReader.getPropValues("MAX_POOL_SIZE")));
			cpds.setMaxStatements(Integer.parseInt(PropertiesReader.getPropValues("MAX_STATEMENTS")));
			cpds.setTestConnectionOnCheckout(true);
			cpds.setMaxIdleTime(Integer.parseInt(PropertiesReader.getPropValues("MAX_IDLE_TIMEOUT")));
			cpds.setMaxIdleTimeExcessConnections(Integer.parseInt(PropertiesReader.getPropValues("MAX_IDLE_TIMEOUT")));
			cpds.setPreferredTestQuery("SELECT 1 from DUAL");
			cpds.setNumHelperThreads(10);


		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}



	public static Connection getPooledDBConn() {
		return createCXLPooledConn();
	}

	private DBUtil() {

	}



	private  static Connection createCXLPooledConn() {

		try {
			pooledCXLDBConn = cpds.getConnection();
		} catch (SQLException ex) {
			logger.error("Error creating ENT DB Connection");

		}
		return pooledCXLDBConn;
	}
	
	public static void close(Statement stmt,ResultSet rs,Connection conn) {
		if(stmt != null)
			try {
				stmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				logger.info("Exception in closing Statement",e);
			}
		if(rs != null)
			try {
				rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				logger.info("Exception in closing ResultSet",e);
			}
		if(conn!=null)
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				logger.info("Exception in closing Connection",e);
			}
	}




}
